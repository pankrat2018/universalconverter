﻿namespace Converter
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxSourceitems = new System.Windows.Forms.ComboBox();
            this.comboBoxConvert = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSource = new System.Windows.Forms.TextBox();
            this.Count = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxRes = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxSourceitems
            // 
            this.comboBoxSourceitems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSourceitems.FormattingEnabled = true;
            this.comboBoxSourceitems.Location = new System.Drawing.Point(12, 22);
            this.comboBoxSourceitems.Name = "comboBoxSourceitems";
            this.comboBoxSourceitems.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSourceitems.TabIndex = 0;
            // 
            // comboBoxConvert
            // 
            this.comboBoxConvert.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxConvert.FormattingEnabled = true;
            this.comboBoxConvert.Location = new System.Drawing.Point(156, 22);
            this.comboBoxConvert.Name = "comboBoxConvert";
            this.comboBoxConvert.Size = new System.Drawing.Size(121, 21);
            this.comboBoxConvert.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Что переводим?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(153, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Во что переводим?";
            // 
            // tbSource
            // 
            this.tbSource.Location = new System.Drawing.Point(15, 71);
            this.tbSource.Name = "tbSource";
            this.tbSource.Size = new System.Drawing.Size(262, 20);
            this.tbSource.TabIndex = 4;
            this.tbSource.TextChanged += new System.EventHandler(this.tbSource_TextChanged);
            this.tbSource.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSource_KeyPress);
            // 
            // Count
            // 
            this.Count.Location = new System.Drawing.Point(12, 141);
            this.Count.Name = "Count";
            this.Count.Size = new System.Drawing.Size(265, 23);
            this.Count.TabIndex = 5;
            this.Count.Text = "Перевести";
            this.Count.UseVisualStyleBackColor = true;
            this.Count.Click += new System.EventHandler(this.Count_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Исходные данные";
            // 
            // textBoxRes
            // 
            this.textBoxRes.Location = new System.Drawing.Point(12, 114);
            this.textBoxRes.Name = "textBoxRes";
            this.textBoxRes.ReadOnly = true;
            this.textBoxRes.Size = new System.Drawing.Size(265, 20);
            this.textBoxRes.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Результат";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 171);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxRes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Count);
            this.Controls.Add(this.tbSource);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxConvert);
            this.Controls.Add(this.comboBoxSourceitems);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Unit Converter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxSourceitems;
        private System.Windows.Forms.ComboBox comboBoxConvert;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSource;
        private System.Windows.Forms.Button Count;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxRes;
        private System.Windows.Forms.Label label4;
    }
}

