﻿using System;
using System.Windows.Forms;

namespace Converter
{
    public partial class Valuta : Form
    {
        public Valuta()
        {
            InitializeComponent();
        }

        private void btnGetCourse_Click(object sender, EventArgs e)
        {
            Parse parse = new Parse();
            parse.GetCourse();
        }

        private void Valuta_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            main f = new main();
            f.ShowDialog();
        }

        private void Valuta_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            textBoxSource.Select();
        }

        private void textBoxSource_TextChanged(object sender, EventArgs e)
        {
            if (textBoxSource.Text == "0")
            {
                textBoxSource.Text = "0,";
                textBoxSource.SelectionStart = textBoxSource.Text.Length;
            }
        }

        private void textBoxSource_KeyPress(object sender, KeyPressEventArgs e)
        {
            char l = e.KeyChar;
            if (!Char.IsDigit(l) && l != '\b')
            {
                if (l != ',' || textBoxSource.Text.IndexOf(",") != -1)
                {
                    e.Handled = true;
                }
            }
            if (l == (char)Keys.Enter)
            {
                buttonCount.PerformClick();
            }
        }

        private void buttonCount_Click(object sender, EventArgs e)
        {
            if (textBoxSource.Text == string.Empty)
            {
                MessageBox.Show("Вы забыли ввести данные для перевода", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            
            
            if (textBoxSource.Text[0] == '0' && textBoxSource.Text.Length == 1)
            {
                textBoxRes.Text = "0";
                return;
            }
            if (textBoxSource.Text[0] == ',' || (textBoxSource.Text[0] == '0' && textBoxSource.Text[1] != ','))
            {
                MessageBox.Show("Некорректный ввод данных", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Parse parse = new Parse();
            string getSource = textBoxSource.Text;
            double source;
            if(double.TryParse(getSource, out source))
            {
                switch(comboBox1.SelectedIndex)
                {
                    case 0:
                        string parseUsd = parse.GetUsd(); // rub -> usd
                        double getUsd;
                        if(double.TryParse(parseUsd, out getUsd))
                        {
                            textBoxRes.Text = Convert.ToString(source / getUsd);
                        }
                        break;
                    case 1:
                        string parseEuro = parse.GetEuro(); // rub -> euro
                        double getEuro;
                        if (double.TryParse(parseEuro, out getEuro))
                        {
                            textBoxRes.Text = Convert.ToString(source / getEuro);
                        }
                        break;
                    case 2:
                        string parseUsdRev = parse.GetUsd(); // usd -> rub
                        double getUsdRev;
                        if (double.TryParse(parseUsdRev, out getUsdRev))
                        {
                            textBoxRes.Text = Convert.ToString(source * getUsdRev);
                        }
                        break;
                    case 3:
                        string parseEuroRev = parse.GetEuro(); // euro -> rub
                        double getEuroRev;
                        if (double.TryParse(parseEuroRev, out getEuroRev))
                        {
                            textBoxRes.Text = Convert.ToString(source * getEuroRev);
                        }
                        break;
                }
            }
            
        }
    }
}
