﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Converter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] source = File.ReadAllLines("config.txt", Encoding.GetEncoding("windows-1251"));
            int i = 0;
           while(source[i] != "[TO]")
            {
                comboBoxSourceitems.Items.Add(source[i]);
                i++;
            }
            for(int j = i+1; j < source.Length; j++)
            {
                comboBoxConvert.Items.Add(source[j]);
            }
            comboBoxSourceitems.SelectedIndex = 0;
            comboBoxConvert.SelectedIndex = 0;
            tbSource.Select();
        }

        private void Count_Click(object sender, EventArgs e)
        {
            if(tbSource.Text == string.Empty)
            {
                MessageBox.Show("Вы забыли ввести данные для перевода", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if(comboBoxConvert.Text.ToLower() == comboBoxSourceitems.Text.ToLower())
            {
                textBoxRes.Text = tbSource.Text;
                return;
            }
            
            if(tbSource.Text[0] == '0' && tbSource.Text.Length == 1)
            {
                textBoxRes.Text = "0";
                return;
            }

            if (tbSource.Text[0] == ',' || (tbSource.Text[0] ==  '0' && tbSource.Text[1] != ','))
            {
                MessageBox.Show("Некорректный ввод данных", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

           // string[] source = File.ReadAllLines("config.txt", Encoding.GetEncoding(1251));
            string[] values = File.ReadAllLines("count.txt", Encoding.GetEncoding(1251));
            
            string text = comboBoxSourceitems.Text.ToLower() + "-" + comboBoxConvert.Text.ToLower(); // формула для поиска в файле
            bool podschet = false;
            for(int i = 0; i < values.Length; i++)
            {
                if (!values[i].Contains("-"))
                {
                    MessageBox.Show("В файле со значениями неверно записаны значения, они должны быть записаны через - (тире) (величина-величина)", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if(values[i].Contains(text))
                {
                    podschet = true;
                    string t = values[i].Replace(text, "").Trim();
                    char znak = t[0];
                    t = t.Replace(znak.ToString(), "");
                    double k;
                    if(double.TryParse(t,out k))
                    {
                        double rez;
                        if (double.TryParse(tbSource.Text, out rez))
                        {
                            switch (znak)
                            {
                                case '*': rez *=  k; textBoxRes.Text = rez.ToString(); break;
                                case '/': rez /= k; textBoxRes.Text = rez.ToString(); break;
                                case '+': rez += k; textBoxRes.Text = rez.ToString(); break;
                                case '-': rez -= k; textBoxRes.Text = rez.ToString(); break;
                                default: MessageBox.Show("Ошибка в формуле для расчёта", "", MessageBoxButtons.OK, MessageBoxIcon.Error); break;
                            }
                        }
                    }

                }
                
                
            }
            if(!podschet)
            {
                MessageBox.Show("Не найдена формула для расчёта", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void tbSource_KeyPress(object sender, KeyPressEventArgs e)
        {
            char l = e.KeyChar;
            if(!Char.IsDigit(l) && l != '\b')
            {
                if (l != ',' || tbSource.Text.IndexOf(",") != -1)
                {
                        e.Handled = true;
                }
            }
            if(l == (char)Keys.Enter)
            {
                Count.PerformClick();
            }

        }

        private void tbSource_TextChanged(object sender, EventArgs e)
        {
            if(tbSource.Text == "0")
            {
                tbSource.Text = "0,";
                tbSource.SelectionStart = tbSource.Text.Length;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            main f = new main();
            f.ShowDialog();
        }
    }
}
