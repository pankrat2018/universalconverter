﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace Converter
{
    class Parse
    {
        private bool CheckFile()
        {
            if (File.Exists("Valute.txt"))
            {

                string[] getCourse = File.ReadAllText("Valute.txt").Split(' ');
                double resUsd, resEuro;
                bool checkUsd = Double.TryParse(getCourse[0], out resUsd);
                bool checkEuro = Double.TryParse(getCourse[1], out resEuro);
                if (getCourse.Length != 5 && !checkUsd && !checkEuro)
                {
                    return false;
                    
                }
                getCourse = null;
                return true;
            }
            else
            {
                return false;
            }
        }
        private void GetFileCourse()
        { 
            
                string[] getCourse = File.ReadAllText("Valute.txt").Split(' ');
                MessageBox.Show("Доллар: " + getCourse[0] + "\n" + "Евро: " + getCourse[1] + "\n" + "Последние обновление курсов валют: " + getCourse[2] + " " + getCourse[3] + " " + getCourse[4]);
                getCourse = null;
            
        }
        private string GetFileCourseUsd()
        {
            string[] getCourse = File.ReadAllText("Valute.txt").Split(' ');
            return getCourse[0];
        }
        private string GetFileCourseEuro()
        {
            string[] getCourse = File.ReadAllText("Valute.txt").Split(' ');
            return getCourse[1];
        }


        public string GetUsd()
        {
            try
            {
                XmlTextReader reader = new XmlTextReader("http://www.cbr.ru/scripts/XML_daily.asp");
                string getusdxml = "";
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:

                            if (reader.Name == "Valute")
                            {
                                if (reader.HasAttributes)
                                {
                                    while (reader.MoveToNextAttribute())
                                    {
                                        if (reader.Name == "ID")
                                        {
                                            if (reader.Value == "R01235") // usd
                                            {
                                                reader.MoveToElement();
                                                getusdxml = reader.ReadOuterXml();
                                            }
                                        }

                                        
                                    }
                                }
                            }

                            break;
                    }

                }
                XmlDocument usdXmlDocument = new XmlDocument();
                usdXmlDocument.LoadXml(getusdxml);
                XmlNode xmlNode = usdXmlDocument.SelectSingleNode("Valute/Value");
                decimal usdValue = Convert.ToDecimal(xmlNode.InnerText);

                return usdValue.ToString();
            }
            catch
            {
                if (CheckFile())
                    return GetFileCourseUsd();
                else return "error";
            }
            
        }
        public string GetEuro()
        {
            try
            {
                XmlTextReader reader = new XmlTextReader("http://www.cbr.ru/scripts/XML_daily.asp");
                string geteuroxml = "";
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:

                            if (reader.Name == "Valute")
                            {
                                if (reader.HasAttributes)
                                {
                                    while (reader.MoveToNextAttribute())
                                    {
                                        if (reader.Name == "ID")
                                        {
                                            if (reader.Value == "R01239") // euro
                                            {
                                                reader.MoveToElement();
                                                geteuroxml = reader.ReadOuterXml();
                                            }
                                        }


                                    }
                                }
                            }

                            break;
                    }

                }
                XmlDocument usdXmlDocument = new XmlDocument();
                usdXmlDocument.LoadXml(geteuroxml);
                XmlNode xmlNode = usdXmlDocument.SelectSingleNode("Valute/Value");
                decimal euroValue = Convert.ToDecimal(xmlNode.InnerText);

                return euroValue.ToString();
            }
            catch
            {
                if (CheckFile())
                    return GetFileCourseEuro();
                else return "error";
            }
        }
        public void GetCourse()
        {
            string euro = GetEuro();
            string usd = GetUsd();
            if(euro != "error" && usd != "error")
            {
                string date = DateTime.Today.ToString("dd MMMM yyyy");
                File.WriteAllText("Valute.txt",usd + " " + euro + " " + date);
                MessageBox.Show("Доллар: " + usd + "\n" + "Евро: " + euro + "\n" + "Последние обновление курсов валют: " + date);
            }
            else
            {
                MessageBox.Show("Ошибка чтения файла, подключитесь к интернету, чтобы скачать свежие курсы валют или верните файл в исходное состояние", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
            
        }
    }
}
