﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Converter
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]

        private static bool GetFiles(string Url, string FileName)
        {
            
            try
            {
                WebClient req = new WebClient();
                req.DownloadFile(Url,FileName);                
                return true;
            }
            catch { return false; }
           
        }

        private static bool GetUpdate(string FileName)
        {
           try
            {
                WebClient client = new WebClient();

                using (Stream stream = client.OpenRead("http://samyrai.esy.es/Converter/" + FileName + ".php"))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string config = File.ReadAllText(FileName + ".txt", Encoding.GetEncoding("UTF-8"));
                        string line = "";
                        while ((line = reader.ReadLine()) != null)
                        {
                            if(!config.Contains(line))
                            {
                                return true;
                            }

                        }
                        return false;

                    }

                }
            }
            catch { return false; }
             
        }
        static void Main()
        {
            if(!File.Exists("config.txt"))
            {
                if(!GetFiles("http://samyrai.esy.es/Converter/config.txt", "config.txt"))
                {
                    MessageBox.Show("В папке с программой отсутствуют необходимые файлы, подключитесь к интернету, или положите файлы в папку с программой,", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                
            }
            
            if (!File.Exists("count.txt"))
            {
                if(!GetFiles("http://samyrai.esy.es/Converter/count.txt", "count.txt"))
                {
                    MessageBox.Show("В папке с программой отсутствуют необходимые файлы, подключитесь к интернету, или положите файлы в папку с программой,","Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            try
            {
                if (GetUpdate("count") || GetUpdate("config"))
                {
                    bool update = MessageBox.Show("Доступно обновление файлов конфигурации, хотите скачать?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
                    if (update)
                    {
                        if(!GetFiles("http://samyrai.esy.es/Converter/config.txt", "config.txt") || !GetFiles("http://samyrai.esy.es/Converter/count.txt", "count.txt"))
                        {
                            MessageBox.Show("Ошибка", "Не удалось скачать обновления файлов конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }                            
                    }
                }
            }
            catch { MessageBox.Show("Ошибка", "Не удалось скачать обновления файлов конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error); }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new main());
        }
    }
}
